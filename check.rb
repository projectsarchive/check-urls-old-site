#!/usr/bin/env ruby
# encoding: utf-8

`chcp 65001`
 
require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'csv'
require 'colorize'
require 'win32console'
require './statustext.rb'
require 'unicode'

skip_on_404 = false
from_line = 0
sample_host = ''
check_host = ''
filename = nil
output_filename = nil
check_first = false

ARGV.each_with_index do |arg, i|
	case arg.downcase.strip
	when '--in'
		filename = ARGV[i+1]
	when '--out'
		output_filename = ARGV[i+1]
	when '--host1'
		sample_host = ARGV[i+1].downcase.strip
	when '--host2'
		check_host = ARGV[i+1].downcase.strip
	when '--from'
		from_line = ARGV[i+1].to_i # Optional, to continue from interuption
	when '--skip404'
		skip_on_404 = true
	when '--first'
		check_first = true
	end
end

def logr(string, line=false)
	string_print = string
	if line 
		string_print = line.to_s + ": #{string_print}"
	end
	return string_print
end

def log(string, line=false)
	puts logr(string, line)
end

result_template = {
	sample_url: "sample_url",
	check_url: "check_url",
	result: "result",
	wrong_code: "wrong_code",
	h1_diff: "h1_diff",
	h2_diff: "h2_diff",
	title_diff: "title_diff",
	sample_code: "sample_code",
	check_code: "check_code",
	sample_h1: "sample_h1",
	check_h1: "check_h1",
	sample_h2: "sample_h2",
	check_h2: "check_h2",
	sample_title: "sample_title",
	check_title: "check_title"
}

def finish_console(status_text, status, messages_array, count) 
	count[status] += 1
	case status
	when :ok then status = status.to_s.upcase.green
	when :warning then status = status.to_s.yellow
	when :error then status = status.to_s.red
	end
	status_text.setText(status)
	status_text.nextLine
	messages_array.each {|message| puts message}
	status_text.nextLine

end

def check_tag(tag, equal_in, equal_to, result_array, messages, result)
	new_result = result
	sample_tag = equal_to.xpath("//#{tag}//text()").first.to_s.strip.encode('UTF-8').gsub(/\s+/, " ")
	sample_tag = Unicode::downcase(sample_tag)
	result_array["sample_#{tag}".to_sym] = sample_tag

	check_tag = equal_in.xpath("//#{tag}//text()").first.to_s.strip.encode('UTF-8').gsub(/\s+/, " ")
	check_tag = Unicode::downcase(check_tag)
	result_array["check_#{tag}".to_sym] = check_tag

	if !check_tag.eql?(sample_tag)
		messages << "Tag content #{tag} '#{check_tag}' should be '#{sample_tag}'"
		new_result = :warning if new_result == :ok
		result_array["#{tag}_diff".to_sym] = "1"
	end
	return new_result
end

results = []
results << result_template.clone

result_template.each { |k, v| result_template[k] = '' }

count = {
	ok: 0,
	warning: 0,
	error: 0,
	skip: 0
}

begin
	CSV.foreach(filename, { col_sep: "\t"}) do |row|
		if from_line > 0 and from_line > $.
			next
		end
		puts $.
		result_array = result_template.clone
		url = row[0]
		messages = Array.new(0,"")

		if !'http'.eql?(url[0,4].downcase)
			log("String '#{url[0,4].downcase}' doesn't start with http".yellow)
			count[:skip] += 1
			next
		end

		url_parts_matches = url.match(/((https?\:\/\/)([^\/]*))(\/?.*)/)
		current_host = url_parts_matches[3].downcase

		if !current_host.eql?(sample_host)
			log("Current host '#{current_host}' differs from sample host '#{sample_host}'. Skipping.".yellow, $.)
			count[:skip] += 1
			next
		end

		current_protocol = url_parts_matches[2]
		current_path = url_parts_matches[4]

		sample_url = url
		check_url = "#{current_protocol}#{check_host}#{current_path}"

		result_array[:sample_url] = sample_url
		result_array[:check_url] = check_url


		#log "Checking '#{check_url}' being same as '#{sample_url}'"

		a = StatusText.new(:before => "'#{sample_url}'...", :after => "")
		a.setText('')

		result = :ok

		sample_code = nil
		begin
			sample_open = open(sample_url)
		rescue OpenURI::HTTPError => e
			if e.message == '404 Not Found'
				if !skip_on_404
					sample_code = 404
				else
					messages << "#{sample_url} is absent (404). Skipping."
					result_array[:sample_code] = "404"
					result = :skip
					result_array[:result] = result.to_s
					results << result_array
					finish_console(a, result, messages, count)
					next
				end
				
			else
				raise e
			end
		end

		sample_code = sample_open.status[0] if !sample_code

		result_array[:sample_code] = sample_code

		if sample_code.to_i != 200
			if skip_on_404
				messages << "#{sample_url} status code is #{sample_code} not 200. Skipping."
				result = :skip
			else
				messages << "#{sample_url} status code is #{sample_code}"
				result = :error
			end				
			result_array[:result] = result.to_s
			results << result_array
			finish_console(a, result, messages, count)
			next
		end

		if !check_first
			begin
				check_open = open(check_url)
			rescue OpenURI::HTTPError => e
				if e.message == '404 Not Found'
					messages << "#{check_url} status code is 404"
					result_array[:wrong_code] = "1"
					result_array[:check_code] = "404"
					result = :error
					result_array[:result] = result.to_s
					results << result_array
					finish_console(a, result, messages, count)
					next
				else
					raise e
				end
			end
			check_code = check_open.status[0]
			result_array[:check_code] = check_code

			if check_code.to_i != 200
				messages << "#{check_url} status code is #{check_code}"
				result_array[:wrong_code] = "1"
				result = :error
				result_array[:result] = result.to_s
				results << result_array
				finish_console(a, result, messages, count)
				next
			end

			sample_html = Nokogiri::HTML(sample_open) if sample_open

			check_html = Nokogiri::HTML(check_open) if check_open

			result = check_tag('h1', check_html, sample_html, result_array, messages, result)
			result = check_tag('h2', check_html, sample_html, result_array, messages, result)
			result = check_tag('title', check_html, sample_html, result_array, messages, result)
		end

		finish_console(a, result, messages, count)

		result_array[:result] = result.to_s
		results << result_array
	end
rescue Exception => e
	puts "Exception: #{e}"
end

puts count

puts "Writing to file..."

if from_line > 0
	type = "ab"
	results.delete_at(0)
else
	type = "wb"
end

CSV.open(output_filename, type) do |csv|
	results.each { |result_row| csv << result_row.values }
end

puts "Finished"